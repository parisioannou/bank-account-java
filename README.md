This source code is part of my studies in the 2nd year of my CS degree. We were asked to develop a simple bank application in Java. Users can see information such as account ID and balance, withdraw/deposit money(if balance>request), savings account offers some allowance for specific deposit thresholds, withdraw fees, interest, and few more activities. 

It contains 4 classes:
1) a Bank class,
2) an abstract class called Account,
3) a SavingAccount class (subclass of the Account class),
4) a CurrentAccount class (subclass of the Account class).

Therefore, we had to showcase the principles of an OOP language. Concepts covered in this project: Abstract classes, Constructors, Class Inheritance, and more simple concepts such as loops, lists etc.

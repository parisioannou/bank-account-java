
public class CurrentAccount extends Account {
	
	//// stores the number of checks used every month
	public int numberOfChecksUsed;
	
	////constructor that gets and initialises attributes from parent class
	public CurrentAccount(String accountID,double initialBalance) {
		super(accountID,initialBalance);
	}
	
	//implementing the withdraw method, it takes out the amount provided from balance if it meets certain criteria
	public boolean withdraw(double amount) {
		if(balance - amount <0) {
			return false;
		}
		
		balance -= (amount-1);
		return true;
	}
	
	//implementing deposit method, adds amount to the account balance, charges a fee
	public void deposit(double amount) {
		balance += (amount-1);
	}
	
	//method that resets the numberOfChecksUsed
	public void resetChecksUSed() {
		this.numberOfChecksUsed = 0;
	}
	
	// This method returns the numberOfChecksUsed
	public int getChecksUsed() {
		return this.numberOfChecksUsed;
	}
	
	//This method allows us to use checks to withdraw cash and charges fee
	public boolean withdrawUsingCheck(double amount) {
		
		if(balance - amount < -10) {
			return false;
		}
		
		if(this.numberOfChecksUsed <= 3) {
			balance -= amount;
		}
		
		if(this.numberOfChecksUsed > 3) {
			balance -= (amount-2);
		}
		
		this.numberOfChecksUsed++;
		
		return true;
	}
	
	////toString method to print information about the account
	public String toString() {
		return "Account ["+super.toString()+ ", Number of checks=" + numberOfChecksUsed + "]";
	}
}

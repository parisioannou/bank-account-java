
public class SavingAccount extends Account {

	public double interest;
	
	//constructor that gets and initialises attributes from parent class and sets some rules
	public SavingAccount(String accountID, double initialDeposit) {
		super(accountID,initialDeposit);
		
		if(initialDeposit >= 1000) {
			this.balance = initialDeposit+10;
		}
	}
	
	//implementing the withdraw method, it takes out the amount provided from balance
	public boolean withdraw(double amount) {
		if(balance - amount <10) {
			return false;
		}
		
		balance -= amount;
		return true;
	}
	
	//implementing deposit method, adds amount to the account balance
	public void deposit(double amount) {
		this.balance += amount;
	}
	
	//method computes the account interest
	public double addInterest(double rate) {
		double interest = balance * (rate/100);
		balance += interest;
		return interest;
		}
	
	//toString method to print information about the account
	public String toString() {
		return "Account ["+super.toString()+ ", Interest Rate=" + this.interest + "]";
	}
}

import java.util.ArrayList;

public class Bank {

		String name;
		double savingsInterestRate;
		static ArrayList<Account> listOfAccounts;
		
		// creating a new ArrayList
		public Bank() {
			listOfAccounts = new ArrayList<Account>();
		}
		
		 // sets the rate in percent for the Savings Account
		public void setSavingsInterestRate(double rate) {
			this.savingsInterestRate=rate;
		}
		
		// deposit the inserted amount into the account specified by accountID
		public boolean deposit(String accountID, double amount) {
			for(int i=0; i < listOfAccounts.size(); i++) {
				if(listOfAccounts.get(i).getAccountID().equals(accountID)) {
					listOfAccounts.get(i).deposit(amount);
					return true;
				}
			}
			return false;
		}
		
		// withdraw the inserted amount from the account specified by accountID
		public boolean withdraw(String accountID, double amount) {
			for(int i=0; i<listOfAccounts.size();i++) {
	               if(listOfAccounts.get(i).getAccountID().equals(accountID)) {
	                   boolean status = listOfAccounts.get(i).withdraw(amount);
	                   return status;
	               }
	           }
	           return false;
		}
		
		//withdraw from fromAccountID and deposit to toAccountID
		public boolean transfer(String fromAccountID, String toAccountID, double amount) {
		    for(int i=0; i<listOfAccounts.size();i++) {
	               if(listOfAccounts.get(i).getAccountID().equals(fromAccountID)) {
	                   for(int j=0; j<listOfAccounts.size();j++) {
	                       if(listOfAccounts.get(j).getAccountID().equals(toAccountID)) {
	                           boolean status = listOfAccounts.get(i).withdraw(amount);
	                           if(status) {
	                        	   listOfAccounts.get(j).deposit(amount);
	                           return true;
	                           }else
	                               return false;
	                       }
	                   }
	               }
	           }
	           return false;
		}
}


public abstract class Account {

	public String accountID;
	public double balance;
	
	//constructor initialises the instance variables
	public Account(String accountID, double balance) {
		this.accountID = accountID;
		this.balance = balance;
	}
	
	//get accountID
	public String getAccountID() {
		return this.accountID;
	}
	
	//get balance
	public double getBalance() {
		return this.balance;
	}
	//toString method to print information about the account such as account ID and balance
	public String toString() {
		return "Account [accountID=" + this.accountID + ", Balance=" + this.balance + "]";
	}
	
	//Withdraw abstract method, it will be used from the different type of accounts, will be overrided
	public abstract boolean withdraw(double amount);
	
	//Deposit abstract method, it will be used from the different type of accounts, will be overrided 
	public abstract void deposit(double amount);
	
	
}
